package edu.grsu.projects.utils;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * User : Vanja Novak
 * Date : 04.11.2015
 * Time : 9:38
 */
@Component
public final class SpringUtils implements ApplicationContextAware {

    private static ApplicationContext context;

    private SpringUtils() {
        // nothing to do
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;
    }

    public static <T> T getBean(String beanName, Class<T> clazz) {
        return (T) context.getBean(beanName, clazz);
    }
}