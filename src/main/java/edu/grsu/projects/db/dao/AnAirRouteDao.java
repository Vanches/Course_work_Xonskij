package edu.grsu.projects.db.dao;
 
import java.util.List;
 
import edu.grsu.projects.db.model.AnAirRoute;

public interface AnAirRouteDao {

	void addAnAirRoute(AnAirRoute anAirRoute);
 
	List<AnAirRoute> findAllAnAirRoute();

	void deleteRoute(Integer id);
 
}