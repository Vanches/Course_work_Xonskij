package edu.grsu.projects.db.dao;

import java.util.List;

import edu.grsu.projects.db.model.User;

public interface UserDao {

    void addUser(User user);

    User login(String login, String pass);

    Boolean checkLogin(String login);

    List<User> findAllUser();

    void deleteUser(Integer id);
}