package edu.grsu.projects.db.dao.impl;

import edu.grsu.projects.db.dao.ReservationTicketDao;
import edu.grsu.projects.db.model.ReservationTicket;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.util.List;

public class ReservationTicketDaoImpl extends HibernateDaoSupport implements ReservationTicketDao {

    public void addReservationTicket(ReservationTicket reservationTicket) {

        getHibernateTemplate().save(reservationTicket);

    }

    public List<ReservationTicket> findAllReservationTicket() {

        return (List<ReservationTicket>) getHibernateTemplate().find("from ReservationTicket ");

    }

    public void deleteTicket(Integer id) {
        getHibernateTemplate().find("delete from ReservationTicket where ticketId=" + id);
    }
}