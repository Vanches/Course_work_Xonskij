package edu.grsu.projects.db.dao.impl;

import edu.grsu.projects.db.dao.UserDao;
import edu.grsu.projects.db.model.User;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import java.util.List;

public class UserDaoImpl extends HibernateDaoSupport implements UserDao {

    public void addUser(User user) {

        getHibernateTemplate().save(user);

    }

    @Override
    public User login(String login, String pass) {

        String sql = "from User where pass ='" + pass + "' and login='" + login + "'";
        List list = getHibernateTemplate().find(sql);

        if (list.size() == 0) {
            return null;
        }
        return (User) list.get(0);
    }

    @Override
    public Boolean checkLogin(String login) {
        List list = getHibernateTemplate().find("from User where login=?", login);
        return list.size() != 0;
    }

    public List<User> findAllUser() {

        return (List<User>) getHibernateTemplate().find("from User ");

    }

    @Override
    public void deleteUser(Integer id) {
        getHibernateTemplate().find("delete from User where userId="+id);
    }
}
