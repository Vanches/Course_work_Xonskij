package edu.grsu.projects.db.dao.impl;

import java.util.List;

import edu.grsu.projects.db.dao.AnAirRouteDao;
import edu.grsu.projects.db.model.AnAirRoute;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

public class AnAirRouteDaoImpl extends HibernateDaoSupport implements AnAirRouteDao {

	public void addAnAirRoute(AnAirRoute anAirRoute){

		getHibernateTemplate().save(anAirRoute);

	}

	public List<AnAirRoute> findAllAnAirRoute(){

		return (List<AnAirRoute>) getHibernateTemplate().find("from AnAirRoute ");

	}

	@Override
	public void deleteRoute(Integer id) {
		getHibernateTemplate().find("delete from AnAirRoute where anAirRouteId="+id);
	}
}