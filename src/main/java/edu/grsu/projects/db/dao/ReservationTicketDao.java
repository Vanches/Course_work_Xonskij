package edu.grsu.projects.db.dao;

import edu.grsu.projects.db.model.ReservationTicket;

import java.util.List;

public interface ReservationTicketDao {
    void addReservationTicket(ReservationTicket reservationTicket);

    List<ReservationTicket> findAllReservationTicket();

    void deleteTicket(Integer id);

}
