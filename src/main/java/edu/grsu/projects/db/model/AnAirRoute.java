package edu.grsu.projects.db.model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "anAirRoute", catalog = "CourseProjects", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
public class AnAirRoute {

	public int anAirRouteId;
	public String nameDeparture;
	public String nameDestination;
	public Date departureDate; // дата вылета
	public String departureTime; // время вылета
	public String durationFlight; // продолжительность полёта
	public Double ticketPriceEconomy;
	public Double ticketPriceBusiness; // стоймость билета бизнесс
	public String aeroCodeAndNameStart; // код аэропорта и название
	public String aeroCodeAndNameEnd;
	public Set<ReservationTicket> reservationTickets = new HashSet<ReservationTicket>(
			0);
	public Airplane airplane;

public AnAirRoute() {
}
	public AnAirRoute(Set<ReservationTicket> reservationTickets, Airplane airplane, String nameDeparture, String nameDestination, Date departureDate, String departureTime, String durationFlight, Double ticketPriceEconomy, Double ticketPriceBusiness, String aeroCodeAndNameStart, String aeroCodeAndNameEnd) {
		this.nameDeparture = nameDeparture;
		this.nameDestination = nameDestination;
		this.departureDate = departureDate;
		this.departureTime = departureTime;
		this.durationFlight = durationFlight;
		this.ticketPriceEconomy = ticketPriceEconomy;
		this.ticketPriceBusiness = ticketPriceBusiness;
		this.aeroCodeAndNameStart = aeroCodeAndNameStart;
		this.aeroCodeAndNameEnd = aeroCodeAndNameEnd;
		this.reservationTickets = reservationTickets;
		this.airplane = airplane;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public int getAnAirRouteId() {
		return anAirRouteId;
	}
	public void setAnAirRouteId(int anAirRouteId) {
		this.anAirRouteId = anAirRouteId;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "anAirRoute")
	public Set<ReservationTicket> getReservationTickets() {
		return this.reservationTickets;
	}
	public void setReservationTickets(Set<ReservationTicket> reservationTickets) {
		this.reservationTickets = reservationTickets;
	}

	@OneToOne(fetch = FetchType.LAZY, mappedBy = "anAirRoute")
	public Airplane getAirplane() {
		return this.airplane;
	}
	public void setAirplane(Airplane airplane) {
		this.airplane = airplane;
	}

	@Column(name = "NAMEDEPARTURE", unique = true, nullable = false)
	public String getNameDeparture() {
		return nameDeparture;
	}
	public void setNameDeparture(String nameDeparture) {this.nameDeparture = nameDeparture;}

	@Column(name = "NAMEDESTINATION", unique = true, nullable = false)
	public String getNameDestination() {
		return nameDestination;
	}
	public void setNameDestination(String nameDestination) {this.nameDestination = nameDestination;}

	@Column(name = "DEPARTUREDATE", unique = true, nullable = false)
	public Date getDepartureDate() {
		return departureDate;
	}
	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	@Column(name = "DEPARTURETIME", unique = true, nullable = false)
	public String getDepartureTime() {return departureTime;}
	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}

	@Column(name = "DURATIONFLIGHT", unique = true, nullable = false)
	public String getDurationFlight() {
		return durationFlight;
	}
	public void setDurationFlight(String durationFlight) {
		this.durationFlight = durationFlight;
	}

	@Column(name = "TICKETPRICEECONOMY", unique = true, nullable = false)
	public Double getTicketPriceEconomy() {
		return ticketPriceEconomy;
	}
	public void setTicketPriceEconomy(Double ticketPriceEconomy) {
		this.ticketPriceEconomy = ticketPriceEconomy;
	}


	@Column(name = "TICKETPRICEBUSINESS", unique = true, nullable = false)
	public Double getTicketPriceBusiness() {
		return ticketPriceBusiness;
	}
	public void setTicketPriceBusiness(Double ticketPriceBusiness) {
		this.ticketPriceBusiness = ticketPriceBusiness;
	}

	@Column(name = "AEROCODEANDNAME", unique = true, nullable = false)
	public String getAeroCodeAndNameStart() {
		return aeroCodeAndNameStart;
	}
	public void setAeroCodeAndNameStart(String aeroCodeAndNameStart) {
		this.aeroCodeAndNameStart = aeroCodeAndNameStart;
	}

	@Column(name = "AEROCODEANDNAMEEND", unique = true, nullable = false)
	public String getAeroCodeAndNameEnd() {
		return aeroCodeAndNameEnd;
	}
	public void setAeroCodeAndNameEnd(String aeroCodeAndNameEnd) {
		this.aeroCodeAndNameEnd = aeroCodeAndNameEnd;
	}



}