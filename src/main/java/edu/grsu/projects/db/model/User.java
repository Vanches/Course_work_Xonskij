package edu.grsu.projects.db.model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "user", catalog = "CourseProjects", uniqueConstraints = {
        @UniqueConstraint(columnNames = "id"),
        @UniqueConstraint(columnNames = "login")
   })

public class User {
    public Integer userId;
    public String login;
    public String pass;
    public String nameUser;
    public String surnameUser;
    public String patronymicUser;
    public String userPol;
    public Date userDate;
    public String userNationality;
    public String userPasportNumber;
    public Integer accessRights;
    public Set<ReservationTicket> reservationTickets = new HashSet<ReservationTicket>(
            0);

    public User() {
    }

    public User(String login, String pass, String nameUser) {
        this.login = login;
        this.pass = pass;
        this.nameUser = nameUser;
    }

    public User(String login, String pass, String nameUser, String surnameUser,
                String patronymicUser, String userPol, Date userDate, String userNationality,
                String userPasportNumber, Integer accessRights, Set<ReservationTicket> reservationTickets) {
        this.login = login;
        this.pass = pass;
        this.nameUser = nameUser;
        this.surnameUser = surnameUser;
        this.patronymicUser = patronymicUser;
        this.userPol = userPol;
        this.userDate = userDate;
        this.userNationality = userNationality;
        this.userPasportNumber = userPasportNumber;
        this.accessRights = accessRights;
        this.reservationTickets = reservationTickets;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    public Integer getUserId() {
        return this.userId;
    }
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Column(name = "NAMEUSER", nullable = false)
    public String getNameUser() {
        return nameUser;
    }
    public void setNameUser(String nameUser) {
        this.nameUser = nameUser;
    }

    @Column(name = "SURNAMEUSER", nullable = false)
    public String getSurnameUser() {
        return surnameUser;
    }
    public void setSurnameUser(String surnameUser) {
        this.surnameUser = surnameUser;
    }

    @Column(name = "PATRONYMICUSE", nullable = false)
    public String getPatronymicUser() {
        return patronymicUser;
    }
    public void setPatronymicUser(String patronymicUser) {
        this.patronymicUser = patronymicUser;
    }

    @Column(name = "USERPOL", nullable = false)
    public String getUserPol() {
        return userPol;
    }
    public void setUserPol(String userPol) {
        this.userPol = userPol;
    }

    @Column(name = "USERDATE", nullable = false)
    public Date getUserDate() {
        return userDate;
    }
    public void setUserDate(Date userDate) {
        this.userDate = userDate;
    }

    @Column(name = "USERNATIONALITY", nullable = false)
    public String getUserNationality() {
        return userNationality;
    }
    public void setUserNationality(String userNationality) {
        this.userNationality = userNationality;
    }

    @Column(name = "USERPASPORTNUMBER", nullable = false)
    public String getUserPasportNumber() {
        return userPasportNumber;
    }
    public void setUserPasportNumber(String userPasportNumber) {
        this.userPasportNumber = userPasportNumber;
    }


    @Column(name = "ACCESSRIGHTS", nullable = false)
    public Integer getAccessRights() {return accessRights;}
    public void setAccessRights(Integer accessRights) {this.accessRights = accessRights;}

    @Column(name = "login", unique = true, nullable = false)
    public String getLogin() {
        return login;
    }
    public void setLogin(String login) {
        this.login = login;
    }

    @Column(name = "pass", nullable = false)
    public String getPass() {
        return pass;
    }
    public void setPass(String pass) {
        this.pass = pass;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
     public Set<ReservationTicket> getReservationTickets() {
        return this.reservationTickets;
    }
    public void setReservationTickets(Set<ReservationTicket> reservationTickets) {
        this.reservationTickets = reservationTickets;
    }
}