package edu.grsu.projects.db.bo.impl;

import java.util.List;

import edu.grsu.projects.db.bo.AnAirRouteBo;
import edu.grsu.projects.db.dao.AnAirRouteDao;
import edu.grsu.projects.db.model.AnAirRoute;

public class AnAirRouteBoImpl implements AnAirRouteBo {

    AnAirRouteDao AnAirRouteDao;

    public void setAnAirRouteDao(AnAirRouteDao anAirRouteDao) {
        this.AnAirRouteDao = anAirRouteDao;
    }

    public void addAnAirRoute(AnAirRoute anAirRoute) {

        AnAirRouteDao.addAnAirRoute(anAirRoute);

    }

    public List<AnAirRoute> findAllAnAirRoute() {

        return AnAirRouteDao.findAllAnAirRoute();
    }

    @Override
    public void deleteRoute(Integer id) {
        AnAirRouteDao.deleteRoute(id);
    }
}