package edu.grsu.projects.db.bo.impl;

import java.util.List;

import edu.grsu.projects.db.bo.UserBo;
import edu.grsu.projects.db.dao.UserDao;
import edu.grsu.projects.db.model.User;

public class UserBoImpl implements UserBo {

    UserDao UserDao;

    public void setUserDao(UserDao userDao) {
        this.UserDao = userDao;
    }

    public void addUser(User user) {

        UserDao.addUser(user);

    }

    @Override
    public User login(String login, String pass) {
        return UserDao.login(login, pass);
    }

    @Override
    public Boolean checkLogin(String login) {
        return UserDao.checkLogin(login);
    }

    public List<User> findAllUser() {

        return UserDao.findAllUser();
    }

    @Override
    public void deleteUser(Integer id) {
        UserDao.deleteUser(id);
    }
}