package edu.grsu.projects.db.bo;
 
import java.util.List;
 
import edu.grsu.projects.db.model.AnAirRoute;
 
public interface AnAirRouteBo {
 
	void addAnAirRoute(AnAirRoute anAirRoute);
 
	List<AnAirRoute> findAllAnAirRoute();

	void deleteRoute(Integer id);
}