package edu.grsu.projects.db.bo;

import edu.grsu.projects.db.model.ReservationTicket;

import java.util.List;

public interface ReservationTicketBo {
    void addReservationTicket(ReservationTicket reservationTicket);

    List<ReservationTicket> findAllReservationTicket();

    void deleteTicket(Integer id);
}
