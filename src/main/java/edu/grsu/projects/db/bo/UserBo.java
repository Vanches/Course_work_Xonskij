package edu.grsu.projects.db.bo;


import edu.grsu.projects.db.model.User;

import java.util.List;

public interface UserBo {

    void addUser(User user);

    User login(String login, String pass);

    Boolean checkLogin(String login);

    List<User> findAllUser();

    void deleteUser(Integer id);
}
