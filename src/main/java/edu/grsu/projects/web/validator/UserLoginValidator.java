package edu.grsu.projects.web.validator;


import edu.grsu.projects.db.bo.UserBo;
import edu.grsu.projects.utils.SpringUtils;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator("edu.grsu.user.loginValidator")
public class UserLoginValidator implements Validator {
    @Override
    public void validate(FacesContext facesContext, UIComponent uiComponent, Object value) throws ValidatorException {
        UserBo userService = SpringUtils.getBean("userBo", UserBo.class);
        if (value instanceof String && userService.checkLogin((String) value)) {
            throw new javax.faces.validator.ValidatorException(new FacesMessage(""));
        }
    }
}