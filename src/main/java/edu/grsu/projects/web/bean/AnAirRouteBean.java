package edu.grsu.projects.web.bean;

import edu.grsu.projects.db.bo.AnAirRouteBo;
import edu.grsu.projects.db.model.Airplane;
import edu.grsu.projects.db.model.AnAirRoute;
import edu.grsu.projects.db.model.ReservationTicket;
import edu.grsu.projects.db.model.User;
import org.primefaces.context.RequestContext;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.event.ActionEvent;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@ManagedBean
@RequestScoped
public class AnAirRouteBean implements Serializable {

    public String nameDeparture;
    public String nameDestination;
    public Date departureDate; // дата вылета
    public String departureTime; // время вылета
    public String durationFlight; // продолжительность полёта
    public Double ticketPriceEconomy; // стоймость билета эконом
    public Double ticketPriceBusiness;
    public String aeroCodeAndNameStart; // дата вылета
    public String aeroCodeAndNameEnd;
    public Set<ReservationTicket> reservationTickets = new HashSet<ReservationTicket>(
            0);
    public Airplane airplane;

    @ManagedProperty(value = "#{anAirRouteBo}")
    private AnAirRouteBo anAirRouteBo;

    List<AnAirRoute> anAirRoutes;

    @PostConstruct
    private void init() {
        anAirRoutes = anAirRouteBo.findAllAnAirRoute();
    }

    public String addAnAirRoute() {
        RequestContext context = RequestContext.getCurrentInstance();
        AnAirRoute anAirRoute = new AnAirRoute(getReservationTickets(), getAirplane(), getNameDeparture(), getNameDestination(), getDepartureDate(),
                getDepartureTime(), getDurationFlight(), getTicketPriceEconomy(), getTicketPriceBusiness(),
                getAeroCodeAndNameStart(), getAeroCodeAndNameEnd());
        try {
            anAirRouteBo.addAnAirRoute(anAirRoute);
        } catch (Exception e) {
        }
        return "index";
    }

    public String deleteAnAirRoute(Integer id){
    anAirRouteBo.deleteRoute(id);
        return null;
    }

    //region GET/SET

    public Set<ReservationTicket> getReservationTickets() {
        return this.reservationTickets;
    }
    public void setReservationTickets(Set<ReservationTicket> reservationTickets) {
        this.reservationTickets = reservationTickets;
    }

    public String getNameDeparture() {
        return nameDeparture;
    }

    public void setNameDeparture(String nameDeparture) {
        this.nameDeparture = nameDeparture;
    }

    public String getNameDestination() {
        return nameDestination;
    }

    public void setNameDestination(String nameDestination) {
        this.nameDestination = nameDestination;
    }

    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public String getDurationFlight() {
        return durationFlight;
    }

    public void setDurationFlight(String durationFlight) {
        this.durationFlight = durationFlight;
    }

    public Double getTicketPriceEconomy() {
        return ticketPriceEconomy;
    }

    public void setTicketPriceEconomy(Double ticketPriceEconomy) {
        this.ticketPriceEconomy = ticketPriceEconomy;
    }

    public Double getTicketPriceBusiness() {
        return ticketPriceBusiness;
    }

    public void setTicketPriceBusiness(Double ticketPriceBusiness) {
        this.ticketPriceBusiness = ticketPriceBusiness;
    }

    public String getAeroCodeAndNameStart() {
        return aeroCodeAndNameStart;
    }

    public void setAeroCodeAndNameStart(String aeroCodeAndNameStart) {
        this.aeroCodeAndNameStart = aeroCodeAndNameStart;
    }

    public String getAeroCodeAndNameEnd() {
        return aeroCodeAndNameEnd;
    }

    public void setAeroCodeAndNameEnd(String aeroCodeAndNameEnd) {
        this.aeroCodeAndNameEnd = aeroCodeAndNameEnd;
    }

    public AnAirRouteBo getAnAirRouteBo() {
        return anAirRouteBo;
    }

    public void setAnAirRouteBo(AnAirRouteBo anAirRouteBo) {
        this.anAirRouteBo = anAirRouteBo;
    }

    public List<AnAirRoute> getAnAirRoutes() {
        return anAirRoutes;
    }

    public Airplane getAirplane() {return this.airplane;}
    public void setAirplane(Airplane airplane) {
        this.airplane = airplane;
    }

//endregion
}
