package edu.grsu.projects.web.bean;

import edu.grsu.projects.db.model.User;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;

@ManagedBean(name = "sessionBean")
@SessionScoped
public class SessionBean implements Serializable {

    private Integer userId;
    private String userLogin;
    private String userName;
    private Integer userAccessRights;

    public String setUserInfo(User user) {
        if (user == null) {
            return null;
        }

        setUserId(user.getUserId());
        setUserLogin(user.getLogin());
        setUserName(user.getNameUser());
        setUserAccessRights(user.getAccessRights());
        return null;
    }

    public String quit() {
        setUserId(null);
        setUserLogin(null);
        setUserName(null);
        setUserAccessRights(null);
        return "index";
    }

    //region GET/SET

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getUserAccessRights() {
        return userAccessRights;
    }

    public void setUserAccessRights(Integer userAccessRights) {
        this.userAccessRights = userAccessRights;
    }


    //endregion

}
