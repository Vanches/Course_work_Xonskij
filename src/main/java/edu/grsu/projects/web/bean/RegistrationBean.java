package edu.grsu.projects.web.bean;

import edu.grsu.projects.db.bo.UserBo;
import edu.grsu.projects.db.model.AnAirRoute;
import edu.grsu.projects.db.model.ReservationTicket;
import edu.grsu.projects.db.model.User;
import org.primefaces.context.RequestContext;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@ManagedBean(name = "regBean")
@RequestScoped
public class RegistrationBean implements Serializable {

    private String userLogin;
    private String userPass;
    private String nameUser;
    public String surnameUser;
    public String patronymicUser;
    public String userPol;
    public Date userDate;
    public String userNationality;
    public String userPasportNumber;
    public Integer accessRights;
    public Set<ReservationTicket> reservationTickets = new HashSet<ReservationTicket>(
            0);

    @ManagedProperty(value = "#{userBo}")
    private UserBo userBo;


    public String reg() {
        RequestContext context = RequestContext.getCurrentInstance();
        User user = new User(getUserLogin(), getUserPass(), getNameUser(), getSurnameUser(),
                getPatronymicUser(), getUserPol(), getUserDate(), getUserNationality(),
                getUserPasportNumber(), getAccessRights(), getReservationTickets());
        try {
            userBo.addUser(user);
        } catch (Exception e) {
        }
        return "index";
    }

    public void checkLogin(FacesContext context,
                           UIComponent toValidate,
                           Object value) throws javax.faces.validator.ValidatorException {
        if (userBo.checkLogin((String) value)) {
            throw new javax.faces.validator.ValidatorException(new FacesMessage(""));
        }
    }

    //region GET/SET

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getUserPass() {
        return userPass;
    }

    public void setUserPass(String userPass) {
        this.userPass = userPass;
    }

    public String getNameUser() {
        return nameUser;
    }

    public void setNameUser(String nameUser) {
        this.nameUser = nameUser;
    }

    public String getSurnameUser() {
        return surnameUser;
    }

    public void setSurnameUser(String surnameUser) {
        this.surnameUser = surnameUser;
    }

    public String getPatronymicUser() {
        return patronymicUser;
    }

    public void setPatronymicUser(String patronymicUser) {
        this.patronymicUser = patronymicUser;
    }

    public String getUserPol() {
        return userPol;
    }

    public void setUserPol(String userPol) {
        this.userPol = userPol;
    }

    public Date getUserDate() {
        return userDate;
    }

    public void setUserDate(Date userDate) {
        this.userDate = userDate;
    }

    public String getUserNationality() {
        return userNationality;
    }

    public void setUserNationality(String userNationality) {
        this.userNationality = userNationality;
    }

    public String getUserPasportNumber() {
        return userPasportNumber;
    }

    public void setUserPasportNumber(String userPasportNumber) {
        this.userPasportNumber = userPasportNumber;
    }

    public Integer getAccessRights() {
        return accessRights;
    }

    public void setAccessRights(Integer accessRights) {
        this.accessRights = accessRights;
    }

    public UserBo getUserBo() {
        return userBo;
    }

    public void setUserBo(UserBo userBo) {
        this.userBo = userBo;
    }

    public Set<ReservationTicket> getReservationTickets() {
        return this.reservationTickets;
    }
    public void setReservationTickets(Set<ReservationTicket> reservationTickets) {
        this.reservationTickets = reservationTickets;
    }

    //endregion
}
