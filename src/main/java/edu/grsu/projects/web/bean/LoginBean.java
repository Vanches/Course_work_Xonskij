package edu.grsu.projects.web.bean;

import edu.grsu.projects.db.bo.UserBo;
import edu.grsu.projects.db.model.User;
import org.primefaces.context.RequestContext;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import java.io.Serializable;

@ManagedBean(name = "loginBean")
@RequestScoped
public class LoginBean implements Serializable {

    private String login;
    private String pass;

    @ManagedProperty(value = "#{sessionBean}")
    private SessionBean sessionBean;
    @ManagedProperty(value = "#{userBo}")
    private UserBo userBo;

    public String doLogin() {

        User user = userBo.login(login, pass);
        FacesMessage message = null;
        RequestContext context = RequestContext.getCurrentInstance();
        boolean loggedIn = false;
        if (user != null) {
            sessionBean.setUserInfo(user);
            loggedIn = true;
        } else {
            loggedIn = false;
            message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Ошибка при авторизации!", "Неверный логин/пароль");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
        context.addCallbackParam("loggedIn", loggedIn);
        return null;
    }


    //region GET/SET

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public void setUserBo(UserBo userBo) {
        this.userBo = userBo;
    }

    public void setSessionBean(SessionBean sessionBean) {
        this.sessionBean = sessionBean;
    }
    //endregion
}
