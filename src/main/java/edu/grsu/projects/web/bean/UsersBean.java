package edu.grsu.projects.web.bean;

import edu.grsu.projects.db.bo.UserBo;
import edu.grsu.projects.db.model.User;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

@ManagedBean(name = "usersBean")
@RequestScoped
public class UsersBean implements Serializable {
    @ManagedProperty(value = "#{userBo}")
    private UserBo userBo;


    private ArrayList<User> users;

    @PostConstruct
    private void init() {
        users = (ArrayList<User>) userBo.findAllUser();
    }

    public String deleteUser(Integer id){
        userBo.deleteUser(id);
        return null;
    }

    //region GET/SET

    public UserBo getUserBo() {
        return userBo;
    }

    public void setUserBo(UserBo userBo) {
        this.userBo = userBo;
    }


    public ArrayList<User> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<User> users) {
        this.users = users;
    }

    //endregion

}
