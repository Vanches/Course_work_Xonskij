package edu.grsu.projects.web.bean;

import edu.grsu.projects.db.bo.ReservationTicketBo;
import edu.grsu.projects.db.model.AnAirRoute;
import edu.grsu.projects.db.model.ReservationTicket;
import edu.grsu.projects.db.model.ReservationTicket;
import edu.grsu.projects.db.model.User;
import org.primefaces.context.RequestContext;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@ManagedBean
@RequestScoped
public class ReservationTicketBean implements Serializable {

    public User user;
    public AnAirRoute anAirRoute;

    @ManagedProperty(value = "#{reservationTicketBo}")
    private ReservationTicketBo reservationTicketBo;

    List<ReservationTicket> reservationTickets;

    @PostConstruct
    private void init() {
        reservationTickets = reservationTicketBo.findAllReservationTicket();
    }

    public String addReservationTicket() {
        RequestContext context = RequestContext.getCurrentInstance();
        ReservationTicket reservationTicket = new ReservationTicket(getUser(),getAnAirRoute());
        try {
            reservationTicketBo.addReservationTicket(reservationTicket);
        } catch (Exception e) {
        }
        return "index";
    }

    public String deleteTicket(Integer id){
        reservationTicketBo.deleteTicket(id);
        return null;
    }

    public ReservationTicketBo getReservationTicketBo() {
        return reservationTicketBo;
    }
    public void setReservationTicketBo(ReservationTicketBo reservationTicketBo) {
        this.reservationTicketBo = reservationTicketBo;
    }

    public AnAirRoute getAnAirRoute() {
        return this.anAirRoute;
    }
    public void setAnAirRoute(AnAirRoute anAirRoute) {
        this.anAirRoute = anAirRoute;
    }

    public User getUser() {
        return this.user;
    }
    public void setUser(User user) {
        this.user = user;
    }

    public List<ReservationTicket> getReservationTickets() {
        return reservationTickets;
    }


//endregion
}